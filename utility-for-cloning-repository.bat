@REM ---------------------------------------------------------------------------
@REM InowAsia utility batch script for cloning repository
@REM Author: Arnauld BIGANZOLI, mailto:biganzoli@laplace.univ-tlse.fr
@REM credits to: ArnauldDev
@REM License: MIT
@REM Date: 2023-07-25
@REM Version: 0.1.0
@REM 
@REM Description:
@REM The purpose of this script is to help you create the logical folder structure
@REM present on the InowAsia GitLab repository, in order to reproduce this 
@REM directory tree locally on your computer.
@REM 
@REM Usage: launch file utility-for-cloning-repository.bat
@REM 
@REM Note: This script is intended to be used with Windows 10/11
@REM   - PATH_FOR_INOWASIA_REPOSITORY
@REM ---------------------------------------------------------------------------

@REM Preparing the cmd window
@ECHO OFF
cls
title InowAsia Utility cloning Batch

@REM Defining variables locally
setlocal EnableExtensions EnableDelayedExpansion

@REM Allow to set color in console
@REM https://stackoverflow.com/questions/2048509/how-to-echo-with-different-colors-in-the-windows-command-line
@REM https://superuser.com/questions/1569594/how-does-delayed-expansion-work-in-a-batch-script
@REM ver : Microsoft Windows [version 10.0.22621.1992]
for /f %%a in ('echo prompt $E ^| cmd') do (
  set "ESC=%%a"
)

@REM ---------------------------------------------------------------------------
@REM Affichage d'un message d'introduction
echo ===========================================================================
echo                       INOWASIA UTILITY CLONING BATCH
echo ===========================================================================
echo !ESC![33m!"!               _____   ______ _       _____   _____ _______  !ESC![0m
echo !ESC![33m!"!              /  _/ | / / __ \ |     / /   | / ___//  _/   | !ESC![0m
echo !ESC![33m!"!              / //  |/ / / / / | /| / / /| | \__ \ / // /| | !ESC![0m
echo !ESC![33m!"!            _/ // /|  / /_/ /| |/ |/ / ___ |___/ // // ___ | !ESC![0m
echo !ESC![33m!"!           /___/_/ |_/\____/ |__/|__/_/  |_/____/___/_/  |_| !ESC![0m
echo.
echo Script for installing repositories shared on the InowAsia project.
echo License MIT @ 2023 ArnauldDev for InowAsia
echo.
echo Create the logical folder structure present on the InowAsia GitLab repository,
echo in order to reproduce the local directory tree on your computer:
ver
echo.
echo Here is the folder tree from https://gitlab.com/inowasia
echo.
echo    o InowAsia
echo        o Conductivity Probe Atlas
echo        o LoRa Single Board Computer Inventory
echo            - AdafruitRFM95_OTAA
echo    - Getting started with InowAsia
echo.
echo ---------------------------------------------------------------------------
@REM echo.
pause

@REM ---------------------------------------------------------------------------
@REM Defining variables for URL repository
@REM ---------------------------------------------------------------------------
set url_repository_gswi=https://gitlab.com/inowasia/getting-started-with-inowasia.git
set url_repository_sbc_adafruit_rfm95=https://gitlab.com/inowasia/lora-sbc-inventory/AdafruitRFM95_OTAA.git

@REM ---------------------------------------------------------------------------
@REM Checking for the presence of the system environment variable
@REM ---------------------------------------------------------------------------
@REM The objective here is to position itself in the directory just before the InowAsia folder.
echo.
if defined PATH_FOR_INOWASIA_REPOSITORY (
    @REM set check_path_user_for_inowasia_repository=1
    echo Path %PATH_FOR_INOWASIA_REPOSITORY% is set in the system environment variables,
    echo into PATH_FOR_INOWASIA_REPOSITORY variable for the user %USERNAME%.
    if not exist !PATH_FOR_INOWASIA_REPOSITORY! (
        mkdir %PATH_FOR_INOWASIA_REPOSITORY%
    )
    cd /D %PATH_FOR_INOWASIA_REPOSITORY%
    cd..
    set destination_path=!cd!
) else (
    @REM Message to display if the system environment variable does not yet exist
    @REM set check_path_user_for_inowasia_repository=0
    echo Environment variable PATH_FOR_INOWASIA_REPOSITORY for the user %USERNAME% does not exist yet.
    @REM echo you need to create it to remember the location of the InowAsia project folder.
    echo.
    echo Enter the path to the root directory you want to use, for your local workspace for InowAsia source files.
    echo.
    set /p destination_path="Path to your root directory workspace for InowAsia: "
    echo.
    echo You have chosen the directory !destination_path!

    @REM Move in tree to destination
    cd /D !destination_path!

    @REM Save the environment variable PATH_FOR_INOWASIA_REPOSITORY
    echo Creation PATH_FOR_INOWASIA_REPOSITORY environment variable with this directory.
    >nul 2>&1 setx PATH_FOR_INOWASIA_REPOSITORY "!destination_path!\InowAsia"
    @REM DEBUG:
    @REM echo errorlevel = %errorlevel%
    echo.
    if "%errorlevel%" == "0" (
        @REM Success creation of PATH_FOR_INOWASIA_REPOSITORY
        echo The creation of the PATH_FOR_INOWASIA_REPOSITORY environment variable has been successfully completed.
    ) else ( 
        @REM echo Failure creation PATH_FOR_INOWASIA_REPOSITORY
        echo Failed to create environment variable "PATH_FOR_INOWASIA_REPOSITORY",
        echo you must manually enter the destination path of the InowAsia project folder from
        echo the "Environment Variables" window, in "User Variables", then restart this script.
        pause
        @REM Open the "Environment Variables" window, then exit the script
        rundll32.exe sysdm.cpl,EditEnvironmentVariables
        @REM Errorlevel=1, if failure to creation PATH_FOR_INOWASIA_REPOSITORY
        exit /b 1
    )
)

echo.
echo Create directory InowAsia and all it's subdirectories in %destination_path%...
if exist %destination_path% (
    mkdir "InowAsia\Conductivity Probe Atlas"
    mkdir "InowAsia\LoRa Single Board Computer Inventory"
)

@REM Go into directory and open in Windows Explorer
set destination_path=%destination_path%\InowAsia
cd /D %destination_path%
start .

echo.
echo InowAsia structure created in the directory %destination_path%
:label_cloning_repository
echo.
echo Select the repository to clone locally:
echo   [0] Exit script without cloning repository
echo   [1] Getting started with InowAsia
echo   [2] LoRa Single Board Computer Inventory - AdafruitRFM95_OTAA
echo   [3] Conductivity Probe Atlas - ...
echo   [4] All this repositories
echo.

@REM DEBUG:
@REM goto label_eof

@REM Read user choice from keyboard
set /p user_cloning_choice="Enter your choice: "

if %user_cloning_choice%==0 (
    @REM Errorlevel=2, if the user has chosen to exit the script
    @REM exit /b 2
    echo You have chosen to leave the script, thank you and see you soon.
    @REM Waits for the specified number of seconds and then exits.
    timeout 5
    goto label_eof
) else if %user_cloning_choice% == 1 (
    echo You have chosen the repository Getting started with InowAsia
    goto label_cloning_repository_gswi
) else if %user_cloning_choice% == 2 (
    echo You have chosen the repository AdafruitRFM95_OTAA into LoRa Single Board Computer Inventory
    goto label_cloning_repository_sbc_adafruit_rfm95
) else if %user_cloning_choice% == 3 (
    echo You have chosen the repository Conductivity Probe Atlas
    goto label_cloning_repository_cpa
) else if %user_cloning_choice% == 4 (
    echo You have chosen all the repositories
    goto label_cloning_all_repository
) else (
    echo.
    echo You have chosen an invalid option.
    echo Please try again.
    echo.
    pause
    @REM @REM Errorlevel=2, if the user has chosen an invalid option.
    @REM exit /b 2
    goto label_cloning_repository
)

@REM ---------------------------------------------------------------------------
@REM Clone the repository if it does not exist
@REM ---------------------------------------------------------------------------
:label_cloning_all_repository

:label_cloning_repository_gswi
echo.
echo ---------------------------------------------------------------------------
@REM Check if repository exists
if not exist "%destination_path%\Getting started with InowAsia" (
    @REM Move in tree to destination
    cd /D %destination_path%

    echo Cloning the repository:
    echo %url_repository_gswi%
    echo in progress...
    echo.
    git clone %url_repository_gswi% "Getting started with InowAsia"
    echo.
    echo Repository cloned successfully.
) else (
    echo The repository Getting started with InowAsia already exists.
)

@REM If user have only chosen the repository Getting started with InowAsia
if %user_cloning_choice% == 1 (
    goto label_eof
)

:label_cloning_repository_sbc_adafruit_rfm95
echo.
echo ---------------------------------------------------------------------------
@REM Check if repository exists
if not exist "%destination_path%\LoRa Single Board Computer Inventory\AdafruitRFM95_OTAA" (
    @REM Move in tree to destination
    cd /D "%destination_path%\LoRa Single Board Computer Inventory"

    echo Cloning the repository:
    echo %url_repository_sbc_adafruit_rfm95%
    echo in progress...
    echo.
    git clone %url_repository_sbc_adafruit_rfm95%
    echo.
    echo Repository cloned successfully.
) else (
    echo The repository AdafruitRFM95_OTAA already exists.
)

@REM If user have only chosen the repository AdafruitRFM95_OTAA
if %user_cloning_choice%==2 (
    goto label_eof
)

:label_cloning_repository_cpa

@REM End of script
:label_eof
echo.
echo ---------------------------------------------------------------------------
echo You will now quit this script which has come to an end.
echo Thank you for using this script.
echo.
pause

@REM Errorlevel=0, is the exit code for sucess
exit /b 0
