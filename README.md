# Getting started with InowAsia

[About the INOWASIA project...](https://inowasia.com/project/)

## Getting started

To make it easy for you to get started with GitLab and InowAsia IoT projects, here's a list of recommended next steps.

## Clone the folder ans files from the GitLab server to your computer

We have created an easy to use tool to allow you to copy one or more parts of this project.

You just have to download the "[utility-for-cloning-repository.bat](utility-for-cloning-repository.bat)" file, then launch this script by letting you guide.

## Contributing

We are open to contributions. Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

MIT © 2023 InowAsia

see <https://opensource.org/license/mit/>
